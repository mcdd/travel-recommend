package dev.mcdd;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

@SpringBootTest
class BackendApplicationTests {

    PasswordEncoder encoder = Pbkdf2PasswordEncoder.defaultsForSpringSecurity_v5_8();

    @Test
    void contextLoads() {
        String encoded = encoder.encode("password1024");
        System.out.println("encoded = " + encoded); //62220177dcc48231c062c3af1a5afc42e7f5aa896f08881780d976784f211b4452fdabe7b046a547806160ea731fcff2
        boolean matches = encoder.matches("password1024", "62220177dcc48231c062c3af1a5afc42e7f5aa896f08881780d976784f211b4452fdabe7b046a547806160ea731fcff2");
        System.out.println("matches = " + matches);
    }

}
