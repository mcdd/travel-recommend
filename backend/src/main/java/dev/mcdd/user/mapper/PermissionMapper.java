package dev.mcdd.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dev.mcdd.user.entity.Permission;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * PermissionMapper
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:29
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    @Results({
        @Result(column = "permission_id", property = "id"),
        @Result(column = "permission_name", property = "name")
    })
    @Select("SELECT * FROM permission WHERE permission_id IN (SELECT permission_id FROM role_permission_map WHERE role_id = #{roleId})")
    List<Permission> selectPermissionsByRoleId(Integer roleId);
}
