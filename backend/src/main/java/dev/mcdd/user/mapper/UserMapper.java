package dev.mcdd.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dev.mcdd.user.entity.User;

/**
 * UserMapper
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:28
 */
public interface UserMapper extends BaseMapper<User> {
}
