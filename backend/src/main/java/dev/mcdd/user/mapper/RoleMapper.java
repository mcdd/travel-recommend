package dev.mcdd.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dev.mcdd.user.entity.Role;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * RoleMapper
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:28
 */
public interface RoleMapper extends BaseMapper<Role> {

    @Results({
        @Result(column = "role_id", property = "id"),
        @Result(column = "role_name", property = "name")
    })
    @Select("SELECT * FROM role WHERE role_id IN (SELECT role_id FROM user_role_map WHERE user_id = #{userId})")
    Role selectRoleByUserId(Long userId);

    @Insert("INSERT INTO user_role_map VALUES (null,#{userId},1)")
    int saveAdminRoleByUserId(Long userId);

    @Insert("INSERT INTO user_role_map VALUES (null,#{userId},2)")
    int saveUserRoleByUserId(Long userId);


}
