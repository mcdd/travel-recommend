package dev.mcdd.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import dev.mcdd.common.constant.RoleType;
import dev.mcdd.user.entity.Role;
import dev.mcdd.user.entity.User;

/**
 * RoleService
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:29
 */
public interface RoleService extends IService<Role> {

    Role loadRoleByUserId(Long userId);

    User authorizationUserRole(User user, RoleType type);

}
