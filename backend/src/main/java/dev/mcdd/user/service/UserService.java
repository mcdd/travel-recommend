package dev.mcdd.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import dev.mcdd.auth.domain.request.RegisterVo;
import dev.mcdd.user.entity.User;

import java.util.Optional;

/**
 * UserService
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:29
 */
public interface UserService extends IService<User> {
    Optional<User> loadUserByText(String text);
    User saveUser(RegisterVo vo);
}
