package dev.mcdd.user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import dev.mcdd.user.entity.Permission;

import java.util.List;

/**
 * PermissionService
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:29
 */
public interface PermissionService extends IService<Permission> {

    List<Permission> loadPermissionsByRoleId(Integer roleId);


}
