package dev.mcdd.user.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import dev.mcdd.user.entity.Permission;
import dev.mcdd.user.exception.PermissionNotfoundException;
import dev.mcdd.user.mapper.PermissionMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PermissionServiceImpl
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:29
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    private final PermissionMapper mapper;

    @Override
    public List<Permission> loadPermissionsByRoleId(Integer roleId) {
        if (roleId == null) {
            throw new IllegalArgumentException("roleId is null");
        }
        List<Permission> permissions = mapper.selectPermissionsByRoleId(roleId);
        if (permissions.isEmpty()) {
            throw new PermissionNotfoundException(String.format("Permission with roleId %s not found", roleId));
        }
        return permissions;
    }

}
