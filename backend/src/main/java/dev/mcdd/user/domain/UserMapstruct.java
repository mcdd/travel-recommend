package dev.mcdd.user.domain;


import dev.mcdd.auth.domain.request.RegisterVo;
import dev.mcdd.user.domain.response.UserInfo;
import dev.mcdd.user.entity.User;
import org.mapstruct.*;

/**
 * UserMapstruct
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:32
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapstruct {

    @Mapping(source = "roleInfo.permissionInfos", target = "role.permissions")
    @Mapping(source = "roleInfo", target = "role")
    User toEntity(UserInfo userInfo);

    @InheritInverseConfiguration(name = "toEntity")
    UserInfo toUserInfo(User user);

    User toEntity(RegisterVo registerVo);

    @InheritInverseConfiguration(name = "toEntity")
    RegisterVo toUserRegisterVo(User user);
}
