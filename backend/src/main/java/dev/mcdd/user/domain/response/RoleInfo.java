package dev.mcdd.user.domain.response;


import java.util.List;

/**
 * RoleInfo
 * DTO for {@link dev.mcdd.user.entity.Role}
 * @author: mcdd1024
 * @date 2025/1/16 05:31
 */
public record RoleInfo(String name, String code, List<PermissionInfo> permissionInfos) {
}
