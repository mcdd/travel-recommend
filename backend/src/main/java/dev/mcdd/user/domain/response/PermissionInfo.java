package dev.mcdd.user.domain.response;


/**
 * PermissionInfo
 * DTO for {@link dev.mcdd.user.entity.Permission}
 * @author: mcdd1024
 * @date 2025/1/16 05:31
 */
public record PermissionInfo(String name, String code) {
}
