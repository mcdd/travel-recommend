package dev.mcdd.user.domain.response;

import jakarta.validation.constraints.Email;


/**
 * UserInfo
 * DTO for {@link dev.mcdd.user.entity.User}
 * @author: mcdd1024
 * @date 2025/1/16 05:31
 */
public record UserInfo(String username, @Email String email, RoleInfo roleInfo) {
}
