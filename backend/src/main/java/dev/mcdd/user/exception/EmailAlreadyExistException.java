package dev.mcdd.user.exception;


/**
 * EmailAlreadyExistException
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:27
 */
public class EmailAlreadyExistException extends RuntimeException {
    public EmailAlreadyExistException(String message) {
        super(message);
    }
}
