package dev.mcdd.user.exception;


/**
 * PermissionNotfoundException
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:27
 */
public class PermissionNotfoundException extends RuntimeException {
    public PermissionNotfoundException(String message) {
        super(message);
    }
}
