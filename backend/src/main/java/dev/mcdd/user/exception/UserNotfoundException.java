package dev.mcdd.user.exception;


/**
 * UserNotfoundException
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:27
 */
public class UserNotfoundException extends RuntimeException {
    public UserNotfoundException(String message) {
        super(message);
    }
}
