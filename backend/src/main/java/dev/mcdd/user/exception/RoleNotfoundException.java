package dev.mcdd.user.exception;


/**
 * RoleNotfoundException
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:27
 */
public class RoleNotfoundException extends RuntimeException {
    public RoleNotfoundException(String message) {
        super(message);
    }
}
