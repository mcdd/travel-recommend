package dev.mcdd.user.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

/**
 * Permission
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:26
 */
@Getter
@Setter
public class Permission {

    @TableId(value = "permission_id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "permission_name")
    private String name;

    private String code;

}
