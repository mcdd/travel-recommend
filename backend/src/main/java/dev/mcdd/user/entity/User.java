package dev.mcdd.user.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import dev.mcdd.common.entity.BaseEntity;
import jakarta.validation.constraints.Email;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * User
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:26
 */
@Getter
@Setter
public class User extends BaseEntity {

    @TableId(value = "user_id", type = IdType.ASSIGN_ID)
    private Long id;

    @TableField(value = "user_name")
    private String username;

    @Email
    private String email;

    @Length(min = 6, max = 20, message = "password should be [6,20]")
    private String password;

    @TableField(exist = false)
    private Role role;

}
