package dev.mcdd.user.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;

import java.util.List;

/**
 * Role
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:26
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role {

    @TableId(value = "role_id",type = IdType.AUTO)
    private Integer id;

    @TableField(value = "role_name")
    private String name;

    private String code;

    @TableField(exist = false)
    private List<Permission> permissions;

}
