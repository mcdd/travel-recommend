package dev.mcdd.security.adapter;


import org.springframework.security.core.GrantedAuthority;

/**
 * SecurityGrantedAuthorityAdapter
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:40
 */
public record SecurityGrantedAuthorityAdapter(String authority) implements GrantedAuthority {

    public static final String ROLE_PREFIX = "ROLE_";
    public static final String PERMISSION_PREFIX = "PERMISSION_";

    @Override
    public String getAuthority() {
        return this.authority;
    }

    public static SecurityGrantedAuthorityAdapter role(String authority) {
        return new SecurityGrantedAuthorityAdapter(ROLE_PREFIX + authority);
    }

    public static SecurityGrantedAuthorityAdapter permission(String authority) {
        return new SecurityGrantedAuthorityAdapter(PERMISSION_PREFIX + authority);
    }
}
