package dev.mcdd.security.adapter;


import dev.mcdd.user.entity.User;
import dev.mcdd.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * SecurityUserDetailsServiceAdapter
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:42
 */
@Service
@RequiredArgsConstructor
public class SecurityUserDetailsServiceAdapter implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String text) throws UsernameNotFoundException {
        Optional<User> userOptional = userService.loadUserByText(text);
        if (userOptional.isPresent()) {
            return new SecurityUserDetailsAdapter(userOptional.get());
        }
        throw new UsernameNotFoundException(text);
    }
}
