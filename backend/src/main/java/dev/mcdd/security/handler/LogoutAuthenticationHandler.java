package dev.mcdd.security.handler;


import dev.mcdd.common.helper.HttpHelper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author mcdd1024
 * @name LogoutAuthenticationHandler
 * @since 2025/1/16 08:27
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class LogoutAuthenticationHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        try (PrintWriter writer = HttpHelper.responseWriterCustomer(response)) {
            String authorization = request.getHeader("Authorization");
            log.info("authorization = {}", authorization);
            writer.write("logout successful");
        }
    }
}
