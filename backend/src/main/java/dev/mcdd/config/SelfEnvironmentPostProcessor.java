package dev.mcdd.config;


import dev.mcdd.common.env.SelfBanner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * SelfEnvironmentPostProcessor
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:15
 */
public class SelfEnvironmentPostProcessor implements EnvironmentPostProcessor {
    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        application.setBanner(new SelfBanner());
    }
}
