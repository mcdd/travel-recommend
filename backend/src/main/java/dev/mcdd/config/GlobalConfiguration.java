package dev.mcdd.config;


import dev.mcdd.filter.SelfFilter01;
import dev.mcdd.listener.SelfListener01;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

/**
 * GlobalConfiguration
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:07
 */
@Configuration
public class GlobalConfiguration {

    @Bean
    public FilterRegistrationBean<SelfFilter01> selfFilter01() {
        FilterRegistrationBean<SelfFilter01> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new SelfFilter01());
        registrationBean.setUrlPatterns(Set.of("/*"));
        return registrationBean;
    }

    @Bean
    public ServletListenerRegistrationBean<SelfListener01> selfListener01() {
        ServletListenerRegistrationBean<SelfListener01> registrationBean = new ServletListenerRegistrationBean<>();
        registrationBean.setListener(new SelfListener01());
        return registrationBean;
    }


}
