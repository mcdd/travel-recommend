package dev.mcdd.filter;


import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * SelfFilter01
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:07
 */
@Slf4j
public class SelfFilter01 extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("SelfFilter01.doFilter");
        super.doFilter(request, response, chain);
    }
}
