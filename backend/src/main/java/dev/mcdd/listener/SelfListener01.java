package dev.mcdd.listener;


import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import lombok.extern.slf4j.Slf4j;

/**
 * SelfListener01
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:08
 */
@Slf4j
public class SelfListener01 implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("SelfListener01.contextInitialized");
        ServletContextListener.super.contextInitialized(sce);
    }
}
