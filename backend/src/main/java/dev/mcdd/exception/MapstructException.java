package dev.mcdd.exception;


/**
 * MapstructException
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:24
 */
public class MapstructException extends RuntimeException {
    public MapstructException(String message) {
        super(message);
    }
}
