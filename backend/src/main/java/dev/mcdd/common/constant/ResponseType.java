package dev.mcdd.common.constant;


/**
 * ResponseType
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:21
 */
public enum ResponseType {
    SUCCESS,
    CLIENT_ERROR,
    SERVER_ERROR,
    FORBIDDEN;
}
