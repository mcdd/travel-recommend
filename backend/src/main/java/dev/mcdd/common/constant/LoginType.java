package dev.mcdd.common.constant;


import lombok.Getter;

/**
 * LoginType
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:46
 */
@Getter
public enum LoginType {

    USERNAME("username"),
    EMAIL("email");

    private final String type;

    LoginType(String type) {
        this.type = type;
    }

}
