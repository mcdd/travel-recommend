package dev.mcdd.common.constant;


import lombok.Getter;

/**
 * RoleType
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:27
 */
@Getter
public enum RoleType {

    ADMIN(1, "100"),
    USER(2, "200");

    private final int id;
    private final String code;

    RoleType(int id, String code) {
        this.id = id;
        this.code = code;
    }

}

