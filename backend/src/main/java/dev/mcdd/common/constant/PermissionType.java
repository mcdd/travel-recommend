package dev.mcdd.common.constant;


import lombok.Getter;

/**
 * PermissionType
 *
 * @author: mcdd1024
 * @date 2025/1/16 05:27
 */
@Getter
public enum PermissionType {

    INSERT(1, "10"),
    DELETE(1, "20"),
    UPDATE(1, "30"),
    SELECT(2, "40");

    private final int id;
    private final String code;

    PermissionType(int id, String code) {
        this.id = id;
        this.code = code;
    }

}
