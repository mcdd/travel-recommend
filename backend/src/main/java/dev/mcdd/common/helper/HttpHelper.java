package dev.mcdd.common.helper;


import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * @author mcdd1024
 * @name HttpHelper
 * @since 2025/1/16 08:59
 */
public abstract class HttpHelper {

    public static PrintWriter responseWriterCustomer(HttpServletResponse response) throws IOException {
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        return response.getWriter();
    }

}
