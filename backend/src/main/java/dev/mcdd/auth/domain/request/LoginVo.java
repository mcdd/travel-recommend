package dev.mcdd.auth.domain.request;


import dev.mcdd.common.constant.LoginType;

/**
 * LoginVo
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:51
 */
public record LoginVo(
    String username,
    String password,
    String email,
    String code,
    LoginType type
) {
}
