package dev.mcdd.auth.domain.request;


import jakarta.validation.constraints.Email;
import org.hibernate.validator.constraints.Length;

/**
 * UserRegisterVo
 * DTO for {@link dev.mcdd.user.entity.User}
 * @author: mcdd1024
 * @date 2025/1/16 05:31
 */
public record RegisterVo(String username, @Email String email,
                         @Length(message = "password should be [6,20]", min = 6, max = 20) String password) {
}
