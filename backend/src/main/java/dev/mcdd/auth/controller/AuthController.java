package dev.mcdd.auth.controller;


import dev.mcdd.auth.domain.request.RegisterVo;
import dev.mcdd.common.json.JsonVo;
import dev.mcdd.exception.MapstructException;
import dev.mcdd.user.domain.UserMapstruct;
import dev.mcdd.user.domain.response.UserInfo;
import dev.mcdd.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * AuthController
 *
 * @author: mcdd1024
 * @date 2025/1/16 06:50
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final UserService service;
    private final UserMapstruct mapstruct;

    @PostMapping
    public JsonVo register(@RequestBody RegisterVo vo) {
        Optional<UserInfo> userInfoOpt = Optional.ofNullable(mapstruct.toUserInfo(service.saveUser(vo)));
        UserInfo userInfo = userInfoOpt.orElseThrow(() -> new MapstructException("data is empty"));
        return JsonVo.success().add("userInfo", userInfo);
    }


}
