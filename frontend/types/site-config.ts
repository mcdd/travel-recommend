export type SiteConfig = {
    name: string;
    description: string;
    ikun: string;
    hideNavbar?: string[];
    navItems: {
        label: string;
        href: string;
    }[];
    navMenuItems: {
        label: string;
        href: string;
    }[];
    links: {
        github: string;
        twitter: string;
        docs: string;
        discord: string;
        sponsor: string;
    };
}; 