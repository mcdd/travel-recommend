import { SiteConfig } from "@/types/site-config";

export const siteConfig: SiteConfig = {
    name: "Travel Recommend",
    description: "来一场说走就走的旅行吧~",
    ikun: "鸡你太美",
    hideNavbar: ["/"],
    navItems: [
        {
            label: "example",
            href: "/example",
        },
        {
            label: "About",
            href: "/about",
        },
    ],
    navMenuItems: [
        {
            label: "Settings",
            href: "/settings",
        },
        {
            label: "Logout",
            href: "/logout",
        },
    ],
    links: {
        github: "https://github.com/frontio-ai/heroui",
        twitter: "https://twitter.com/hero_ui",
        docs: "https://heroui.com",
        discord: "https://discord.gg/9b6yyZKmH4",
        sponsor: "https://patreon.com/jrgarciadev",
    },
};
