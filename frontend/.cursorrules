# Project background

This project is a Next.js application that uses Tailwind CSS for styling. It is built with TypeScript and follows a component-based architecture. The project is designed to be scalable and maintainable, with a focus on performance and accessibility.

The important thing is that I hope to learn about HeroUI through this project

# Key technologies
- Next.js 14 (app directory)
- HeroUI v2
- TypeScript
- Tailwind CSS
- next-themes

# Project structure
/app - Next.js application pages and layouts
/components - Reusable React components
/config - Configuration files
/public - Static assets
/styles - Global styles
/types - TypeScript type definitions

# Coding standards
- Use TypeScript for type safety
- Follow component-based architecture
- Implement responsive design
- Ensure accessibility standards
- Maintain clean code practices

# Development focus
- Learning and implementing HeroUI components
- Understanding Next.js 14 app directory structure
- Mastering TypeScript with React
- Exploring Tailwind CSS best practices

# Notes
- Keep components modular and reusable
- Document component usage
- Consider performance optimizations
- Follow HeroUI design patterns