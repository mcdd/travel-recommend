import { Link } from "@heroui/link";
import { Code } from "@heroui/code";

export const Footer = () => {
    return (
        <footer className="w-full flex items-center justify-center py-3">
            <Link
                isExternal
                className="flex items-center gap-1 text-current"
                href="https://gitcode.com/mcdd"
                title="heroui.com homepage"
            >
                <span className="text-default-600">Powered by</span>
                <Code color="danger">MCDD 1024</Code>
            </Link>
        </footer>
    );
};
