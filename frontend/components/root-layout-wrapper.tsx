"use client";

import clsx from "clsx";
import { usePathname } from "next/navigation";

import { Providers } from "@/app/providers";
import { siteConfig } from "@/config/site";
import { Navbar } from "@/components/navbar";
import { Footer } from "@/components/footer";

export function RootLayoutWrapper({ children }: { children: React.ReactNode }) {
    const pathname = usePathname();
    const showNavbar = !siteConfig.hideNavbar?.includes(pathname);
    return (
        <Providers themeProps={{ attribute: "class", defaultTheme: "dark" }}>
            <div className="relative flex flex-col h-screen">
                {showNavbar && <Navbar />}
                <main className={clsx("container mx-auto max-w-7xl px-6 flex-grow", showNavbar ? "pt-16" : "pt-0")}>
                    {children}
                </main>
                <Footer />
            </div>
        </Providers>
    );
}
