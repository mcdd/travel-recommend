"use client";
import NextLink from "next/link";
import { usePathname } from "next/navigation";

export default function ExamplePage() {
    const path = usePathname();
    return (
        <div className="flex gap-2">
            <NextLink href={path + "/table"}>table</NextLink>
            <NextLink href={path + "/button"}>button</NextLink>
        </div>
    );
}
