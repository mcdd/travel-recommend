import "@/styles/globals.css";
import { Metadata, Viewport } from "next";
import { RootLayoutWrapper } from "@/components/root-layout-wrapper";
import { siteConfig } from "@/config/site";
import { fontSans } from "@/config/fonts";
import clsx from "clsx";

export const metadata: Metadata = {
    title: {
        default: siteConfig.name,
        template: `%s - ${siteConfig.name}`,
    },
    description: siteConfig.description,
    icons: {
        icon: "/favicon.ico",
    },
};

export const viewport: Viewport = {
    themeColor: [
        { media: "(prefers-color-scheme: light)", color: "white" },
        { media: "(prefers-color-scheme: dark)", color: "black" },
    ],
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
    return (
        <html lang="en" suppressHydrationWarning>
            <body className={clsx("min-h-screen bg-background font-sans antialiased", fontSans.variable)}>
                <RootLayoutWrapper>{children}</RootLayoutWrapper>
            </body>
        </html>
    );
}
