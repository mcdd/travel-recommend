import { button as buttonStyles } from "@heroui/theme";

import { siteConfig } from "@/config/site";
import { title } from "@/components/primitives";
import { DiscordIcon, GithubIcon, TwitterIcon } from "@/components/icons";
import { Button } from "@heroui/button";
import NextLink from "next/link";

export default function Home() {
    return (
        <section className="flex flex-col items-center justify-center gap-4 py-8 md:py-10">
            <div className="inline-block max-w-xl text-center justify-center">
                <span className={title()}>Travel&nbsp;</span>
                <span className={title({ color: "violet" })}>recommend&nbsp;</span>
                <br />
                <span className={title()}>{siteConfig.description}</span>
            </div>

            <div className="flex gap-3 mt-8">
                <NextLink href={"/about"}>
                    <Button
                        className={buttonStyles({
                            color: "danger",
                            radius: "full",
                            variant: "shadow",
                        })}
                    >
                        <GithubIcon size={20} />
                        鸡你太美
                    </Button>
                </NextLink>
            </div>
        </section>
    );
}
