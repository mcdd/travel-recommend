import { Button } from "@heroui/button";
import { button as buttonStyles } from "@heroui/theme";
import { DiscordIcon, GithubIcon, TwitterIcon } from "@/components/icons";

export default function AboutPage() {
    return (
        <div>
            <Button
                className={buttonStyles({
                    color: "danger",
                    radius: "full",
                    variant: "shadow",
                })}
            >
                <GithubIcon size={20} />
                鸡你太美
            </Button>
            <Button
                className={buttonStyles({
                    color: "success",
                    radius: "full",
                    variant: "shadow",
                })}
            >
                <TwitterIcon size={20} />
                你是我的我是你的谁
            </Button>
            <Button
                className={buttonStyles({
                    color: "warning",
                    radius: "full",
                    variant: "shadow",
                })}
            >
                <DiscordIcon size={20} />
                再多一眼看一眼就会爆炸
            </Button>
        </div>
    );
}
