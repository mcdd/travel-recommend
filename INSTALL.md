# backend

```xml
<repositories>
        <repository>
            <id>spring</id>
            <url>https://maven.aliyun.com/repository/spring</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
```

# frontend

```shell
corepack enable
npm config set registry https://registry.npmmirror.com
```

# env

```shell
cd env/docker
docker compose up -d
```